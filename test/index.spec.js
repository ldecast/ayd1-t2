const request = require('supertest');
const app = require('../src/app');

describe('GET /', () => {
    test('Debe mandar un saludo desde el servidor', async () => {
        const result = await request(app).get('/').send();
        expect(result.statusCode).toBe(200);
    })

    test('Debe retornar un arreglo de estudiantes', async () => {
        const result = await request(app).get('/estudiantes').send();
        expect(result.body.estudiantes).toBeDefined();
    })

    test('Debe retornar un arreglo de asignaciones', async () => {
        const result = await request(app).get('/asignaciones').send();
        expect(result.body.asignaciones).toBeDefined();
    })
});

describe('POST /registro', () => {
    test('Debe retornar error 422 por falta de parámetros', async () => {
        const result = await request(app).post('/registro').send({});
        expect(result.statusCode).toBe(422);
    })

    let req = {
        id: 'ldecast',
        name: 'Luis Danniel',
        surname: 'Castellanos',
        email: 'luis.danniel@hotmail.com',
        date: '18/06/2022',
        password: '',
        confirm_password: ''
    };

    test('Debe retornar error 400 porque las contraseñas no coinciden', async () => {
        req.password = '1234';
        req.confirm_password = '0000';
        const result = await request(app).post('/registro').send(req);
        expect(result.statusCode).toBe(400);
    })

    test('Debe retornar código 201 de estudiante creado', async () => {
        req.password = '1234';
        req.confirm_password = '1234';
        const result = await request(app).post('/registro').send(req);
        expect(result.statusCode).toBe(201);
    })

    test('Debe retornar prohibición 403 porque usuario ya existe', async () => {
        const result = await request(app).post('/registro').send(req);
        expect(result.statusCode).toBe(403);
    })
});

describe('POST /login', () => {
    test('Debe retornar error 422 por falta de parámetros', async () => {
        const result = await request(app).post('/login').send({});
        expect(result.statusCode).toBe(422);
    })

    test('Debe retornar código 202 de acceso correcto', async () => {
        let req = { email: 'luis.danniel@hotmail.com', password: '1234' };
        const result = await request(app).post('/login').send(req);
        expect(result.statusCode).toBe(202);
    })

    test('Debe retornar error 401 porque la contraseña es incorrecta', async () => {
        let req = { email: 'luis.danniel@hotmail.com', password: '0000' };
        req.password = '12345';
        const result = await request(app).post('/login').send(req);
        expect(result.statusCode).toBe(401);
    })

    test('Debe retornar error 403 porque el usuario con ese correo no existe', async () => {
        let req = { email: 'luis.danniel@gmail.com', password: '1234' };
        const result = await request(app).post('/login').send(req);
        expect(result.statusCode).toBe(403);
    })
});

describe('POST /asignar', () => {
    test('Debe retornar error 422 por falta de parámetros', async () => {
        const result = await request(app).post('/asignar').send({});
        expect(result.statusCode).toBe(422);
    })

    let req = {
        id: '0283',
        student: 'ldecast',
        subject: 'Análisis y Diseño 1',
        section: 'A',
        day: '18/06/2022',
        hour: '19:00-21:00'
    };

    test('Debe retornar código 201 de asignación creada', async () => {
        const result = await request(app).post('/asignar').send(req);
        expect(result.statusCode).toBe(201);
    })

    test('Debe retornar error 401 ya que usuario no existe', async () => {
        req.student = 'luisd'
        const result = await request(app).post('/asignar').send(req);
        expect(result.statusCode).toBe(401);
    })
});