const express = require('express');
const cors = require('cors');

/* Endpoints */
const Endpoints = require('./endpoints');

/* Init */
// const PORT = 8080;
const app = express();

/* Middlewares */
app.use(express.json());
app.use(cors());

/* Router */
app.use('/', Endpoints);

module.exports = app;